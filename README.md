# Meltano Dashboard

For demo see [HellowWorld.vue](https://gitlab.com/meltano/dashboard/blob/master/src/components/HelloWorld.vue)

[![npm version](https://img.shields.io/npm/v/meltano-dashboard.svg)](https://www.npmjs.com/package/meltano-dashboard)

![Screen Shot](./public/ss.png)

Check Out [`HelloWorld.vue`](https://gitlab.com/meltano/dashboard/blob/master/src/components/HelloWorld.vue) for example usage.

## CSS

### `dashboard-columns`
A row of charts. 

### `dashboard-column`
An individual chart container

### Grid
The grid is flexbox based and divided into 12 columns with 12 different classes. 1 size for each division. 

* `is-2`
* `is-3`
* `is-4`
* `is-5`
* `is-6`
* `is-7`
* `is-8`
* `is-9`
* `is-10`
* `is-11`
* `is-12`

### Dashboard Sizes
To create a small, medium or large dashboard, use the sizes.

* `small`
* `medium`
* `large`

```html
<div class="dashboard-column is-6 medium">
  <div class="chart-board number-board">
    <number-chart
      number="$92,000"
      title="acv"></number-chart>
  </div>
</div>
```

### Chart Containers
Wrap your chart in a `.chart-board` class to get that dashboard look. 

```html
<div class="dashboard-column is-6 medium">
  <div class="chart-board number-board">
    <number-chart
      number="$92,000"
      title="acv"></number-chart>
  </div>
</div>
```

`.chart-board`
Wrapper for the chart itself to create that actual board look.

`.number-board`
Wrapper for number charts to nicely display number charts

When using a `number-chart` add `.number-board` class to center the number in the board.

## Vue Components

### `BarChart`
A basic vertical bar chart

### `HorzBarChart`
A basic horizontal bar chart

### `LineChart`
A basic unfilled line chart

### `AreaChart`
A basic filled line chart

### `NumberChart`
A chart for displaying numerical information in large text.

#### `number` (required, `String`)
The number to display. Any `String`

#### `title` (required, `String`)
The title to display on the card.

### Any Chart
All charts can take a `chart-data` and `chart-labels` property. See `HelloWorld.vue` for an example application.

#### `legendLabel`
When `datasets` is just an array of numbers, set the legend label to this string. In more complex data, passed as `label` in object of dataset.

#### `title` (optional, `String`)
The title of the chart

#### `stacked` (optional, `Boolean`)
For stacked charts

#### `datasets` (optional, `[Number]`, `[Object]`)
The data for the chart itself. See chart.js documentation. 

#### `labels` (optional, `[String]`)
The labels for the chart itself.

```html
<div class="dashboard-columns">
  <div class="dashboard-column is-6 medium">
    <div class="chart-board number-board">
      <number-chart
        number="$92,000"
        title="acv"></number-chart>
    </div>
  </div>
  <div class="dashboard-column is-6 medium">
    <div class="chart-board">
      <bar-chart
        legend-label="months"
        :stacked="false"
        :datasets="multipleSets"
        :labels="chartLabels"></bar-chart>
    </div>
  </div>
</div>
```

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Lints and fixes files
```
yarn run lint
```

### Run your unit tests
```
yarn run test:unit
```

### Run your end-to-end tests
```
yarn run test:e2e
```

